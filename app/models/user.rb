class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :messages
    validates :email, presence: true
    validates :passcode, presence: true
    validates :phone_number, presence: true    

    def initialize(args={})
      super
      self.passcode = args[:passcode]
      if self.passcode == "ADMIN"
        self.admin = true
      end
    end


end
