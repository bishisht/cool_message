class Message < ActiveRecord::Base
  after_initialize :defaults
  belongs_to :user
  validates :message, presence: true
  validates :number, presence: true

  def defaults
    self.status = "Pending" if self.status.nil?
  end
end
