class MessagesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_message, only: [:show, :destroy]
  def index
    @messages = Message.where(user_id: current_user)
    # @messages = Message.all
    # @messages = Message.find_by_user_id
    # redirect_to users_dashboard_path
  end

  def show
    @message = Message.find(params[:id])
  end


  def new
    # @message = Message.new
    @message = current_user.messages.build
  end

  def create
    @message = current_user.messages.build(message_params)
    if @message.save
      redirect_to @message
    else
      render 'new'
    end
  end

  def edit
    @message = Message.find(params[:id])
  end

  def update
    @message = Message.find(params[:id])
    if @message.update(message_params)
      redirect_to @message
    else
      render 'edit'
    end
  end

  def destroy
    @message = Message.find(params[:id])
    @message.destroy
    redirect_to message_path
  end

  def delete
  end


  private
  def find_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.require(:message).permit(:message, :number)
  end
end
