class AdminController < ApplicationController
  before_action :authenticate_user!
  before_action :check_user_status
  def index
    # @messages = Message.all(:order => "created_at DESC")
    @messages = Message.order('created_at DESC')
  end

  def update_status
    message_id=params[:message_id]
    message_status = params[:message_status]
    message = Message.find(message_id)
    message.update_attribute(:status, message_status)
    if message.status == "sent"
      # Get twilio-ruby from twilio.com/docs/ruby/install
      # require 'rubygems'          # This line not needed for ruby > 1.8
      require 'twilio-ruby'

      # Get your Account Sid and Auth Token from twilio.com/user/account
      account_sid = 'AC4bea599df32efae66e6a1bb2121b54c5'
      auth_token = 'baeb6f34b5c039732dc88b96609d9080'
      @client = Twilio::REST::Client.new account_sid, auth_token

      api_message = @client.account.messages.create(:body => "#{current_user.email} says #{message.message}",
          :to => "#{message.number}",
          :from => "+19283251127")
      # puts message.to
      @post_status= api_message.status
      message.status = @post_status
      flash[:notice] = "Message status: #{message_status}"


    end
    # message.update_attribute(:status, post_status)
    redirect_to admin_index_path
  end


  private
  def check_user_status
    if current_user.admin == false
      redirect_to new_user_session_path
    end
  end
end
