class UsersController < ApplicationController
  before_action :authenticate_user!
  def dashboard
  end
  private
  def user_params
    params.require()
    params.require(:user).permit(:email, :password, :password_confirmation, :passcode, :phone_number)
  end
end
