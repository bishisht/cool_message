Rails.application.routes.draw do
  # post 'admin#update_status'
  post 'admin/update_status', to: 'admin#update_status'
  get 'admin/index'
  devise_for :users, :controllers => { registrations: 'registrations' }
  get 'users/dashboard'
  resources :messages
  get 'home/index'
  root 'home#index'
end
